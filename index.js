const express = require("express");
const port = 4000;
const app = express();

app.get("/", (req, res) => {
  res.json("<h1>CD/CI Pipelines</h1>");
});

app.listen(port, () => {
  console.log(`SERVER RUNNINNG AT ${port}`);
});
